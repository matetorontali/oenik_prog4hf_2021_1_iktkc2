﻿// <copyright file="GenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace CD_Store.Repository
{
    using CD_Store.Data;
    using CD_Store.Data.Models;

    /// <summary>
    /// Contains the database entity repositories.
    /// </summary>
    public class GenericRepository
    {
        private readonly LemezDbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository"/> class.
        /// Constructor, what creates the entity repositories with the same context.
        /// </summary>
        public GenericRepository()
        {
            this.ctx = new LemezDbContext();
            this.Bolt = new Repository<Bolt>(this.ctx);
            this.Bolt_Album = new Repository<Bolt_album>(this.ctx);
            this.Lemez = new Repository<Lemez>(this.ctx);
            this.Vasarlas = new Repository<Vasarlas>(this.ctx);
            this.Vevo = new Repository<Vevo>(this.ctx);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository"/> class, with mock IRepositories.
        /// </summary>
        /// <param name="mockBolt">The mock repository for Bolt.</param>
        /// <param name="mockBolt_Album">The mock repository for BoltAlbum.</param>
        /// <param name="mockLemez">The mock repository for Lemez.</param>
        /// <param name="mockVasarlas">The mock repository for Vasarlas.</param>
        /// <param name="mockVevo">The mock repository for trans.</param>
        public GenericRepository(
            IRepository<Bolt> mockBolt,
            IRepository<Bolt_album> mockBolt_Album,
            IRepository<Lemez> mockLemez,
            IRepository<Vasarlas> mockVasarlas,
            IRepository<Vevo> mockVevo)
        {
            this.Bolt = mockBolt;
            this.Bolt_Album = mockBolt_Album;
            this.Lemez = mockLemez;
            this.Vasarlas = mockVasarlas;
            this.Vevo = mockVevo;
        }

        /// <summary>
        /// Gets or sets the getter and setter for Bolt.
        /// </summary>
        public IRepository<Bolt> Bolt { get; set; }

        /// <summary>
        /// Gets or sets the getter and setter for BoltAlbum.
        /// </summary>
        public IRepository<Bolt_album> Bolt_Album { get; set; }

        /// <summary>
        /// Gets or sets the getter and setter for Lemez.
        /// </summary>
        public IRepository<Lemez> Lemez { get; set; }

        /// <summary>
        /// Gets or sets the getter and setter for Vasarlas.
        /// </summary>
        public IRepository<Vasarlas> Vasarlas { get; set; }

        /// <summary>
        /// Gets or sets the getter and setter for Vevo.
        /// </summary>
        public IRepository<Vevo> Vevo { get; set; }
    }
}
