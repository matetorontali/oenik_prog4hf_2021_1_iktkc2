﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace CD_Store.Repository
{
    using System;
    using System.Linq;
    using CD_Store.Data;

    /// <summary>
    /// Contains the database CRUD methods.
    /// </summary>
    /// <typeparam name="T">Type of database entity class.</typeparam>
    public class Repository<T> : IRepository<T>
        where T : class
    {
        private /* static*/ readonly LemezDbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        public Repository()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="context"><see cref="LemezDbContext"/> class, what every other repository shares.</param>
        public Repository(LemezDbContext context)
        {
            this.ctx = context;
        }

        /// <summary>
        /// Insert a new object to the table.
        /// The table is determined by the object's type.
        /// </summary>
        /// <param name="obj">The insertable object.</param>
        public void Insert(T obj)
        {
            this.ctx.Set<T>().Add(obj);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Update a specified table object.
        /// The table is determined by the object's type.
        /// </summary>
        /// <param name="obj">The updated object.</param>
        /// <param name="key">The key of the updateable object.</param>
        public void Update(T obj, object key)
        {
            var old = this.ctx.Set<T>().Find(key);

            if (old == null)
            {
                Console.WriteLine("Ilyen kulcs nincs");
            }
            else
            {
                this.ctx.Entry(old).CurrentValues.SetValues(obj);
                this.ctx.SaveChanges();
            }
        }

        /// <summary>
        /// Delete an object from the table.
        /// The table is determined by the object's type.
        /// </summary>
        /// <param name="obj">The deletable object.</param>
        public void Delete(T obj)
        {
            this.ctx.Set<T>().Remove(obj);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Get every row from a table.
        /// </summary>
        /// <returns><see cref="IQueryable{T}"/>.</returns>
        public virtual IQueryable<T> GetAll() => this.ctx.Set<T>().AsQueryable();

        /// <summary>
        /// Returns a row by ID from the table.
        /// </summary>
        /// <param name="id">The object's ID.</param>
        /// <returns><see cref="T"/>.</returns>
        public T GetByID(object id)
        {
            return this.ctx.Set<T>().Find(id);
        }
    }
}
