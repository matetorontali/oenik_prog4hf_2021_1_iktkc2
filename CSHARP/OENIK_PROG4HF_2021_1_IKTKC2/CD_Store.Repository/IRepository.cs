﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace CD_Store.Repository
{
    using System.Linq;

    /// <summary>
    /// Contains the requirement methods for repository T class.
    /// </summary>
    /// <typeparam name="T">Type of database entity class.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Inserts an object to a database table.
        /// </summary>
        /// <param name="obj">The insertable object.</param>
        void Insert(T obj);

        /// <summary>
        /// Updates an object from a database table.
        /// </summary>
        /// <param name="obj">The updated object.</param>
        /// <param name="key">The updated object's ID.</param>
        void Update(T obj, object key);

        /// <summary>
        /// Deletes an object from table.
        /// </summary>
        /// <param name="obj">The deletable object.</param>
        void Delete(T obj);

        /// <summary>
        /// Returns a row by ID from the table.
        /// </summary>
        /// <param name="id">The object's ID.</param>
        /// <returns><see cref="T"/>.</returns>
        T GetByID(object id);

        /// <summary>
        /// Returns every row from the table.
        /// </summary>
        /// <returns><see cref="IQueryable{T}"/>.</returns>
        IQueryable<T> GetAll();
    }
}
