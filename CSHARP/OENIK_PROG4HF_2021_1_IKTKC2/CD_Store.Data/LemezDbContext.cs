﻿// <copyright file="LemezDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Data
{
    using System;
    using System.IO;
    using CD_Store.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Project's DbContext class.
    /// </summary>
    public class LemezDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LemezDbContext"/> class.
        /// Project's DbContext class's constructor.
        /// </summary>
        public LemezDbContext()
        {
            this.Database.EnsureCreated();
        }

        private static string DbFilePath => Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..\\..\\..\\..\\CD_Store.Data\\LemezAdatB.mdf"));

        /// <summary>
        /// Gets or sets dbSet for bolt class.
        /// </summary>
#pragma warning disable SA1202 // Elements should be ordered by access
        public DbSet<Bolt> Boltok { get; set; }
#pragma warning restore SA1202 // Elements should be ordered by access

        /// <summary>
        /// Gets or sets dbSet for bolt_album class.
        /// </summary>
        public DbSet<Bolt_album> BoltAlbumok { get; set; }

        /// <summary>
        /// Gets or sets dbSet for lemez class.
        /// </summary>
        public DbSet<Lemez> Lemezek { get; set; }

        /// <summary>
        /// Gets or sets dbSet for vasarlas class.
        /// </summary>
        public DbSet<Vasarlas> Vasarlasok { get; set; }

        /// <summary>
        /// Gets or sets dbSet for vevo class.
        /// </summary>
        public DbSet<Vevo> Vevok { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=" + DbFilePath + ";Integrated Security=True; Connect Timeout=3600;");
        }
    }
}
