﻿// <copyright file="vasarlas.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Data.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Gets or sets and sets the Vasarlas.
    /// </summary>
    [Table("vasarlas")]
    public class Vasarlas
    {
        /// <summary>
        /// Gets or sets and sets the Vasarlas Vasarlas_ID.
        /// </summary>
        [Key]
        public int Vasarlas_ID { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas Vasarlas_datum.
        /// </summary>
        public DateTime? Vasarlas_datum { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas V_ID.
        /// </summary>
        [ForeignKey("vevo")]
        public int? V_ID { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas BA_ID.
        /// </summary>
        [ForeignKey("bolt_album")]
        public int? BA_ID { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas Bolt_album.
        /// </summary>
        public virtual Bolt_album Bolt_album { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas Vevo.
        /// </summary>
        public virtual Vevo Vevo { get; set; }
    }
}
