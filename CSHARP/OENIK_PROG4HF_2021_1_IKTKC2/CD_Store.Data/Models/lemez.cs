﻿// <copyright file="lemez.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Data.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Gets or sets and sets the Lemez.
    /// </summary>
    [Table("lemez")]
    public class Lemez
    {
        /// <summary>
        /// Gets or sets the Lemez Album.
        /// </summary>
        [Key]
        public string Album { get; set; }

        /// <summary>
        /// Gets or sets the Lemez Eloado.
        /// </summary>
        public string Eloado { get; set; }

        /// <summary>
        /// Gets or sets the Lemez Stílus.
        /// </summary>
        public string Stílus { get; set; }

        /// <summary>
        /// Gets or sets the Lemez Vonalkod.
        /// </summary>
        public int? Vonalkod { get; set; }

        /// <summary>
        /// Gets or sets the Lemez Kiado.
        /// </summary>
        public string Kiado { get; set; }

        /// <summary>
        /// Gets or sets the Lemez Boritotkeszitette.
        /// </summary>
        public string Boritotkeszitette { get; set; }

        /// <summary>
        /// Gets or sets the Lemez Bolt_album.
        /// </summary>
        public virtual ICollection<Bolt_album> Bolt_album { get; set; }
    }
}
