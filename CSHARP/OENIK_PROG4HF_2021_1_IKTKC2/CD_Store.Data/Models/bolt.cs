﻿// <copyright file="bolt.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Data.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Gets or sets and sets the Bolt.
    /// </summary>
    [Table("bolt")]
    public class Bolt
    {
        /// <summary>
        /// Gets or sets and sets the Bolt Bolt_ID.
        /// </summary>
        [Key]
        public int Bolt_ID { get; set; }

        /// <summary>
        /// Gets or sets and sets the Bolt Nev.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets and sets the Bolt Cim.
        /// </summary>
        public string Cim { get; set; }

        /// <summary>
        /// Gets or sets and sets the Bolt Adoazonosito.
        /// </summary>
        public int? Adoazonosito { get; set; }

        /// <summary>
        /// Gets or sets and sets the Bolt Telefonszam.
        /// </summary>
        public int? Telefonszam { get; set; }

        /// <summary>
        /// Gets or sets and sets the Bolt Tulajdonos.
        /// </summary>
        private string tulajdonos;

        /// <summary>
        /// Gets or sets and sets the Bolt Bolt_album.
        /// </summary>
        [ForeignKey("bolt_album")]
        public virtual ICollection<Bolt_album> Bolt_album { get; set; }

        /// <summary>
        /// Gets tulajdonos.
        /// </summary>
        /// <returns>tulajdonos</returns>
        public string GetTulajdonos()
        {
            return this.tulajdonos;
        }

        /// <summary>
        /// Sets tulajdonos.
        /// </summary>
        public void SetTulajdonos(string value)
        {
            this.tulajdonos = value;
        }
    }
}
