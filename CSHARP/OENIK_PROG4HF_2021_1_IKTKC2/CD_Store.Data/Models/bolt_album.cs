﻿// <copyright file="bolt_album.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Data.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Gets or sets and sets the Bolt_album.
    /// </summary>
    [Table("bolt_album")]
    public class Bolt_album
    {
        /// <summary>
        /// Gets or sets and sets the Vasarlas BA_ID.
        /// </summary>
        [Key]
        public int BA_ID { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas Ar.
        /// </summary>
        public int? Ar { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas Album.
        /// </summary>
        [ForeignKey("lemez")]
        public string Album { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas Bolt_ID.
        /// </summary>
        [ForeignKey("bolt")]
        public int? Bolt_ID { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas Készlet.
        /// </summary>
        public int? Készlet { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas Bolt.
        /// </summary>
        public virtual Bolt Bolt { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas Lemez.
        /// </summary>
        public virtual Lemez Lemez { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vasarlas Vasarlas.
        /// </summary>
        public virtual ICollection<Vasarlas> Vasarlas { get; set; }
    }
}
