﻿// <copyright file="vevo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Data.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Gets or sets and sets the Vevo.
    /// </summary>
    [Table("vevo")]
    public class Vevo
    {
        /// <summary>
        /// Gets or sets and sets the Vevos V_ID.
        /// </summary>
        [Key]
        public int V_ID { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vevos Nev.
        /// </summary>
        public string Nev { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vevos Cim.
        /// </summary>
        public string Cim { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vevos Kor.
        /// </summary>
        public int? Kor { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vevos Gyerekszam.
        public int? Gyerekszam { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vevos Szulhely.
        /// </summary>
        public string Szulhely { get; set; }

        /// <summary>
        /// Gets or sets and sets the Vevos Vasarlas.
        /// </summary>
        public virtual ICollection<Vasarlas> Vasarlas { get; set; }
    }
}
