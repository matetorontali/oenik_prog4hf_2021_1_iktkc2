﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:Elements should appear in the correct order", Justification = "It's not needed", Scope = "member", Target = "~F:CD_Store.Data.Models.Bolt.tulajdonos")]
[assembly: SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1629:Documentation text should end with a period", Justification = "It's not needed", Scope = "member", Target = "~M:CD_Store.Data.Models.Bolt.GetTulajdonos~System.String")]
