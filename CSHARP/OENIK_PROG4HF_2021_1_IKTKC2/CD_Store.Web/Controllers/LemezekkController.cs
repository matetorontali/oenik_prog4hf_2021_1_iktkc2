﻿// <copyright file="LemezekkController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using CD_Store.Data;
    using CD_Store.Data.Models;
    using CD_Store.Logic;
    using CD_Store.Web.Models;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Controller.
    /// </summary>
    public class LemezekkController : Controller
    {
        private readonly ILogic logic;
        private readonly IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="LemezekkController"/> class.
        /// </summary>
        /// <param name="logic">Logic.</param>
        /// <param name="mapper">Mapper.</param>
        public LemezekkController(ILogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Indexer.
        /// </summary>
        /// <returns>View.</returns>
        public IActionResult Index()
        {
            var list = logic.GetLemezek().Select(lemez => mapper.Map<LemezViewModel>(lemez)).ToList();
            return View(list);
        }

        /// <summary>
        /// Details.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <returns>View.</returns>
        public IActionResult Details(string id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            var lemez = logic.GetLemezek().FirstOrDefault(m => m.Album == id);

            if (lemez == null)
            {
                return this.NotFound();
            }

            return this.View(this.mapper.Map<LemezViewModel>(lemez));
        }

        /// <summary>
        /// Creator.
        /// </summary>
        /// <returns>View.</returns>
        public IActionResult Create()
        {
            return this.View();
        }

        /// <summary>
        /// Creator.
        /// </summary>
        /// <param name="lemez">Lemez.</param>
        /// <returns>View.</returns>
        [HttpPost]
        public IActionResult Create(LemezViewModel lemez)
        {
            if (this.ModelState.IsValid)
            {
                logic.Insert(this.mapper.Map<Lemez>(lemez));
                return this.RedirectToAction(nameof(this.Index));
            }

            return this.View(lemez);
        }

        /// <summary>
        /// Editor.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <returns>View.</returns>
        public IActionResult Edit(string id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            var lemez = logic.GetLemezek().FirstOrDefault(m => m.Album == id);
            if (lemez == null)
            {
                return this.NotFound();
            }

            return this.View(this.mapper.Map<LemezViewModel>(lemez));
        }

        /// <summary>
        /// Editor.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <param name="lemez">Lemez.</param>
        /// <returns>View.</returns>
        [HttpPost]
        public IActionResult Edit(string id, LemezViewModel lemez)
        {
            if (id != lemez.Album)
            {
                return this.NotFound();
            }

            if (this.ModelState.IsValid)
            {
                this.logic.Update(this.mapper.Map<Lemez>(lemez), id);
                return this.RedirectToAction(nameof(this.Index));
            }

            return this.View(lemez);
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <returns>View.</returns>
        public IActionResult Delete(string id)
        {
            if (id == null)
            {
                return this.NotFound();
            }

            var lemez = this.logic.GetLemezek().FirstOrDefault(m => m.Album == id);
            if (lemez == null)
            {
                return this.NotFound();
            }

            return this.View(this.mapper.Map<LemezViewModel>(lemez));
        }

        /// <summary>
        /// Delete.
        /// </summary>
        /// <param name="id">ID.</param>
        /// <returns>View.</returns>
        [HttpPost]
        [ActionName("Delete")]
        public IActionResult DeleteConfirmed(string id)
        {
            var lemez = logic.GetLemezek().FirstOrDefault(m => m.Album == id);
            logic.Delete(lemez);
            return this.RedirectToAction(nameof(this.Index));
        }
    }
}
