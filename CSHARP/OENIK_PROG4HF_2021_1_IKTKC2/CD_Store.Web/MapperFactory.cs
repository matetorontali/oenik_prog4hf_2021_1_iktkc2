﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// MapperFactory class.
    /// </summary>
    public class MapperFactory
    {
        /// <summary>
        /// Creates mapping.
        /// </summary>
        /// <returns>Config.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Models.Lemez, Web.Models.LemezViewModel>()
                    .ReverseMap()
                    .ForMember(a => a.Bolt_album, src => src.Ignore());
            });

            return config.CreateMapper();
        }
    }
}