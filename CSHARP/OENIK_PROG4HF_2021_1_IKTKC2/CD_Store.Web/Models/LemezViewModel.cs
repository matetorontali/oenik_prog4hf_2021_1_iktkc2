﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CD_Store.Web.Models
{
    public class LemezViewModel
    {
        [Display(Name = "Lemez album")]
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string Album { get; set; }

        [Display(Name = "Lemez előadója")]
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string Eloado { get; set; }

        [Display(Name = "Lemez stílusa")]
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string Stílus { get; set; }

        [Display(Name = "Lemez vonalkódja")]
        [Required]
        public int? Vonalkod { get; set; }

        [Display(Name = "Lemez kiadója")]
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string Kiado { get; set; }

        [Display(Name = "Lemez borítóját készítette")]
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string Boritotkeszitette { get; set; }
    }
}