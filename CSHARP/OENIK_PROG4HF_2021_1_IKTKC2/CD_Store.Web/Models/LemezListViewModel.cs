﻿namespace CD_Store.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// ListViewmodel.
    /// </summary>
    public class LemezListViewModel
    {
        /// <summary>
        /// Gets or sets a list.
        /// </summary>
        public List<LemezViewModel> LemezLista { get; set; }

        /// <summary>
        /// Gets or sets a lemez.
        /// </summary>
        public LemezViewModel SzerkesztettLemez { get; set; }
    }
}
