namespace CD_Store.Web.Models
{
    /// <summary>
    /// Error.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Gets or sets ID.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets a value indicating whether show ID.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);
    }
}
