﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG4HF_2021_1_IKTKC2
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CD_Store.Data.Models;
    using CD_Store.Logic;

    /// <summary>
    /// The main program class.
    /// </summary>
    internal class Program
    {
        private static Logic logic;

        private static void Main(string[] args)
        {
            logic = new Logic();
            logic.GetBoltok().ForEach(a => Console.WriteLine(a.Cim));
            string beolvasott = string.Empty;
            do
            {
                Console.WriteLine("Írja be a megfelelő számot!");
                Console.WriteLine("1-Boltok");
                Console.WriteLine("2-Bolt_Album");
                Console.WriteLine("3-Lemez");
                Console.WriteLine("4-Vásárlás");
                Console.WriteLine("5-Vevő");
                Console.WriteLine("6-Első NonCrud");
                Console.WriteLine("7-Második NonCrud");
                Console.WriteLine("8-Harmadik NonCrud");
                Console.WriteLine("9-Java");
                Console.WriteLine("10-Insert a Boltban");
                Console.WriteLine("11-Update a Boltban");
                Console.WriteLine("12-Delete a Boltban");
                Console.WriteLine("13-Insert a Bolt_Albumban");
                Console.WriteLine("14-Update a Bolt_Albumban");
                Console.WriteLine("15-Delete a Bolt_Albumban");
                Console.WriteLine("16-Insert a Lemezben");
                Console.WriteLine("17-Update a lemezben");
                Console.WriteLine("18-Delete a Lemezben");
                Console.WriteLine("19-Insert a Vasarlasban");
                Console.WriteLine("20-Update a Vasaralasban");
                Console.WriteLine("21-Delete a Vasarlasban");
                Console.WriteLine("22-Insert a Vevoben");
                Console.WriteLine("23-Update a Vevoben");
                Console.WriteLine("24-Delete a vevoben");
                Console.WriteLine("25-Program bezárása");

                beolvasott = Console.ReadLine();

                if (Convert.ToInt32(beolvasott) == 1)
                {
                    Listazo(logic.GetBoltok());
                }
                else if (Convert.ToInt32(beolvasott) == 2)
                {
                    Listazo(logic.GetBolt_Album());
                }
                else if (Convert.ToInt32(beolvasott) == 3)
                {
                    Listazo(logic.GetLemezek());
                }
                else if (Convert.ToInt32(beolvasott) == 4)
                {
                    Listazo(logic.GetVasarlas());
                }
                else if (Convert.ToInt32(beolvasott) == 5)
                {
                    Listazo(logic.GetVevok());
                }
                else if (Convert.ToInt32(beolvasott) == 6)
                {
                    Listazo(logic.NonCrud1());
                }
                else if (Convert.ToInt32(beolvasott) == 7)
                {
                    Listazo(logic.NonCrud2());
                }
                else if (Convert.ToInt32(beolvasott) == 8)
                {
                    Listazo(logic.NonCrud3());
                }
                else if (Convert.ToInt32(beolvasott) == 9)
                {
                    Console.WriteLine("Adjon meg egy termékszámot:");
                    Listazo(logic.Java(int.Parse(Console.ReadLine())));
                }
                else if (Convert.ToInt32(beolvasott) == 10)
                {
                    Console.WriteLine("Új bolt felvitele");
                    var ujElem = new Bolt();
                    Random r = new Random();

                    Console.WriteLine("ID:");
                    ujElem.Bolt_ID = int.Parse(Console.ReadLine());
                    Console.WriteLine("Név:");
                    ujElem.Nev = Console.ReadLine();
                    Console.WriteLine("Cím:");
                    ujElem.Cim = Console.ReadLine();
                    Console.WriteLine("Adóazonosító:");
                    ujElem.Adoazonosito = int.Parse(Console.ReadLine());
                    Console.WriteLine("Telefonszám:");
                    ujElem.Telefonszam = int.Parse(Console.ReadLine());
                    Console.WriteLine("Tulajdonos:");
                    ujElem.SetTulajdonos(Console.ReadLine());

                    logic.Insert(ujElem);

                    var list = logic.GetBoltok();
                    Console.WriteLine("Sikeres Insert!");
                }
                else if (Convert.ToInt32(beolvasott) == 11)
                {
                    Console.WriteLine("Bolt módosítása");
                    var lista = logic.GetBoltok();
                    Listazo(lista);

                    Console.WriteLine("Adja meg a módosítani kívánt elem sorszámát:");
                    int index = int.Parse(Console.ReadLine());

                    var modositando = lista[index - 1];

                    Console.WriteLine("Név:");
                    modositando.Nev = Console.ReadLine();
                    Console.WriteLine("Cím:");
                    modositando.Cim = Console.ReadLine();
                    Console.WriteLine("Adóazonosító:");
                    modositando.Adoazonosito = int.Parse(Console.ReadLine());
                    Console.WriteLine("Telefonszám:");
                    modositando.Telefonszam = int.Parse(Console.ReadLine());
                    Console.WriteLine("Tulajdonos:");
                    modositando.SetTulajdonos(Console.ReadLine());

                    logic.Update(modositando, modositando.Bolt_ID);
                    Console.WriteLine("Sikeres Update!");
                }
                else if (Convert.ToInt32(beolvasott) == 12)
                {
                    Console.WriteLine("Bolt törlése");
                    var lista = logic.GetBoltok();
                    Listazo(lista);

                    Console.WriteLine("Adja meg a törölni kívánt elem sorszámát:");
                    int index = int.Parse(Console.ReadLine());

                    logic.Delete(lista[index - 1]);
                }
                else if (Convert.ToInt32(beolvasott) == 13)
                {
                    Console.WriteLine("Új bolt_album felvitele");
                    var ujElem = new Bolt_album();

                    Console.WriteLine("BA_ID:");
                    ujElem.BA_ID = int.Parse(Console.ReadLine());
                    Console.WriteLine("Ár:");
                    ujElem.Ar = int.Parse(Console.ReadLine());
                    Console.WriteLine("Album:");
                    ujElem.Album = Console.ReadLine();
                    Console.WriteLine("Készlet:");
                    ujElem.Készlet = int.Parse(Console.ReadLine());
                    Console.WriteLine("Bolt_ID:");
                    ujElem.Bolt_ID = int.Parse(Console.ReadLine());

                    logic.Insert(ujElem);
                }
                else if (Convert.ToInt32(beolvasott) == 14)
                {
                    Console.WriteLine("Bolt_album módosítása");
                    var lista = logic.GetBolt_Album();
                    Listazo(lista);

                    Console.WriteLine("Adja meg a módosítani kívánt elem sorszámát:");
                    int index = int.Parse(Console.ReadLine());

                    var modositando = lista[index - 1];

                    Console.WriteLine("Ár:");
                    modositando.Ar = int.Parse(Console.ReadLine());
                    Console.WriteLine("Album:");
                    modositando.Album = Console.ReadLine();
                    Console.WriteLine("Készlet:");
                    modositando.Készlet = int.Parse(Console.ReadLine());
                    Console.WriteLine("Bolt_ID:");
                    modositando.Bolt_ID = int.Parse(Console.ReadLine());

                    logic.Update(modositando, modositando.BA_ID);
                }
                else if (Convert.ToInt32(beolvasott) == 15)
                {
                    Console.WriteLine("Bolt_album törlése");
                    var lista = logic.GetBolt_Album();
                    Listazo(lista);

                    Console.WriteLine("Adja meg a törölni kívánt elem sorszámát:");
                    int index = int.Parse(Console.ReadLine());

                    logic.Delete(lista[index - 1]);
                }
                else if (Convert.ToInt32(beolvasott) == 16)
                {
                    Console.WriteLine("Új lemez felvitele");
                    var ujElem = new Lemez();

                    Console.WriteLine("Előadó:");
                    ujElem.Eloado = Console.ReadLine();
                    Console.WriteLine("Album:");
                    ujElem.Album = Console.ReadLine();
                    Console.WriteLine("Stílus:");
                    ujElem.Stílus = Console.ReadLine();
                    Console.WriteLine("Vonalkód:");
                    ujElem.Vonalkod = int.Parse(Console.ReadLine());
                    Console.WriteLine("Kiadó:");
                    ujElem.Kiado = Console.ReadLine();
                    Console.WriteLine("Borítót készítette:");
                    ujElem.Boritotkeszitette = Console.ReadLine();

                    logic.Insert(ujElem);
                }
                else if (Convert.ToInt32(beolvasott) == 17)
                {
                    Console.WriteLine("Lemez módosítása");
                    var lista = logic.GetLemezek();
                    Listazo(lista);

                    Console.WriteLine("Adja meg a módosítani kívánt elem sorszámát:");
                    int index = int.Parse(Console.ReadLine());

                    var modositando = lista[index - 1];

                    Console.WriteLine("Előadó:");
                    modositando.Eloado = Console.ReadLine();
                    Console.WriteLine("Album:");
                    modositando.Album = Console.ReadLine();
                    Console.WriteLine("Stílus:");
                    modositando.Stílus = Console.ReadLine();
                    Console.WriteLine("Vonalkód:");
                    modositando.Vonalkod = int.Parse(Console.ReadLine());
                    Console.WriteLine("Kiadó:");
                    modositando.Kiado = Console.ReadLine();
                    Console.WriteLine("Borítót készítette:");
                    modositando.Boritotkeszitette = Console.ReadLine();

                    logic.Update(modositando, modositando.Album);
                }
                else if (Convert.ToInt32(beolvasott) == 18)
                {
                    Console.WriteLine("Lemez törlése");
                    var lista = logic.GetLemezek();
                    Listazo(lista);

                    Console.WriteLine("Adja meg a törölni kívánt elem sorszámát:");
                    int index = int.Parse(Console.ReadLine());

                    logic.Delete(lista[index - 1]);
                }
                else if (Convert.ToInt32(beolvasott) == 19)
                {
                    Console.WriteLine("Új vásárlás felvitele");
                    var ujElem = new Vasarlas();

                    Console.WriteLine("ID:");
                    ujElem.Vasarlas_ID = int.Parse(Console.ReadLine());
                    Console.WriteLine("VásárlóID:");
                    ujElem.V_ID = int.Parse(Console.ReadLine());
                    Console.WriteLine("BoltAlbumID:");
                    ujElem.BA_ID = int.Parse(Console.ReadLine());
                    Console.WriteLine("Vásárlás dátuma:");
                    ujElem.Vasarlas_datum = DateTime.Parse(Console.ReadLine());

                    logic.Insert(ujElem);
                }
                else if (Convert.ToInt32(beolvasott) == 20)
                {
                    Console.WriteLine("Vásárlás módosítása");
                    var lista = logic.GetVasarlas();
                    Listazo(lista);

                    Console.WriteLine("Adja meg a módosítani kívánt elem sorszámát:");
                    int index = int.Parse(Console.ReadLine());

                    var modositando = lista[index - 1];

                    Console.WriteLine("VásárlóID:");
                    modositando.V_ID = int.Parse(Console.ReadLine());
                    Console.WriteLine("BoltAlbumID:");
                    modositando.BA_ID = int.Parse(Console.ReadLine());
                    Console.WriteLine("Vásárlás dátuma:");
                    modositando.Vasarlas_datum = DateTime.Parse(Console.ReadLine());

                    logic.Update(modositando, modositando.Vasarlas_ID);
                }
                else if (Convert.ToInt32(beolvasott) == 21)
                {
                    Console.WriteLine("Vásárlás törlése");
                    var lista = logic.GetVasarlas();
                    Listazo(lista);

                    Console.WriteLine("Adja meg a törölni kívánt elem sorszámát:");
                    int index = int.Parse(Console.ReadLine());

                    logic.Delete(lista[index - 1]);
                }
                else if (Convert.ToInt32(beolvasott) == 22)
                {
                    Console.WriteLine("Új vevő felvitele");
                    var ujElem = new Vevo();

                    Console.WriteLine("ID:");
                    ujElem.V_ID = int.Parse(Console.ReadLine());
                    Console.WriteLine("Név:");
                    ujElem.Nev = Console.ReadLine();
                    Console.WriteLine("Cim:");
                    ujElem.Cim = Console.ReadLine();
                    Console.WriteLine("Kor:");
                    ujElem.Kor = int.Parse(Console.ReadLine());
                    Console.WriteLine("Gyerekek száma:");
                    ujElem.Gyerekszam = int.Parse(Console.ReadLine());
                    Console.WriteLine("Születési hely:");
                    ujElem.Szulhely = Console.ReadLine();

                    logic.Insert(ujElem);
                }
                else if (Convert.ToInt32(beolvasott) == 23)
                {
                    Console.WriteLine("Vevő módosítása");
                    var lista = logic.GetVevok();
                    Listazo(lista);

                    Console.WriteLine("Adja meg a módosítani kívánt elem sorszámát:");
                    int index = int.Parse(Console.ReadLine());

                    var modositando = lista[index - 1];

                    Console.WriteLine("Név:");
                    modositando.Nev = Console.ReadLine();
                    Console.WriteLine("Cim:");
                    modositando.Cim = Console.ReadLine();
                    Console.WriteLine("Kor:");
                    modositando.Kor = int.Parse(Console.ReadLine());
                    Console.WriteLine("Gyerekek száma:");
                    modositando.Gyerekszam = int.Parse(Console.ReadLine());
                    Console.WriteLine("Születési hely:");
                    modositando.Szulhely = Console.ReadLine();

                    logic.Update(modositando, modositando.V_ID);
                }
                else if (Convert.ToInt32(beolvasott) == 24)
                {
                    Console.WriteLine("Vevő törlése");
                    var lista = logic.GetVevok();
                    Listazo(lista);

                    Console.WriteLine("Adja meg a törölni kívánt elem sorszámát:");
                    int index = int.Parse(Console.ReadLine());

                    logic.Delete(lista[index - 1]);
                }
                else if (Convert.ToInt32(beolvasott) == 25)
                {
                    Environment.Exit(0);
                }
                else
                {
                    Console.WriteLine("Hibás szám!");
                }

                Console.ReadKey();
                Console.Clear();
            }
            while (beolvasott != "-1");
        }

        private static void Listazo<T>(List<T> list)
        {
            if (list.Any())
            {
                Console.Clear();
                var properties = typeof(T).GetProperties().Where(a => !a.PropertyType.Name.Contains("ICollection") && a.PropertyType.Namespace == "System").ToList();

                Console.Write("No.;");
                foreach (var x in properties)
                {
                    Console.Write(x.Name + "  ;  ");
                }

                Console.WriteLine();

                int idx = 1;
                foreach (var x in list)
                {
                    Console.Write(idx + ";");
                    foreach (var prop in properties)
                    {
                        Console.Write(prop.GetValue(x, null) + "  ;  ");
                    }

                    Console.WriteLine();
                    idx++;
                }
            }
            else
            {
                Console.WriteLine("Lista üres!");
            }
        }
    }
}
