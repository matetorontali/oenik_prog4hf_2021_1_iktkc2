﻿using CD_Store.Data.Models;
using CD_Store.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WpfApp.Command;

namespace WpfApp.ViewModels
{
    public class BoltFormVM
    {
        private readonly ILogic logic;
        public BoltFormVM(ILogic logic)
        {
            this.logic = logic;
        }

        public BoltFormVM(ILogic logic, int bolt_id)
        {
            this.logic = logic;
            var bolt = logic.GetBoltok().FirstOrDefault(a => a.Bolt_ID == bolt_id);

            if (bolt != null)
            {
                Bolt_ID = bolt.Bolt_ID;
                Nev = bolt.Nev;
                Cim = bolt.Cim;
                Adoazonosito = bolt.Adoazonosito;
                Telefonszam = bolt.Telefonszam;
            }
        }
        public int Bolt_ID { get; set; }

        public string Nev { get; set; }

        public string Cim { get; set; }

        public int? Adoazonosito { get; set; }

        public int? Telefonszam { get; set; }

        public void SaveBolt()
        {
            if (Bolt_ID == 0)
            {
                logic.Insert(new Bolt
                {
                    Bolt_ID = Bolt_ID,
                    Adoazonosito = Adoazonosito,
                    Cim = Cim,
                    Nev = Nev,
                    Telefonszam = Telefonszam
                });
            }
            else
            {

                logic.Update(new Bolt
                {
                    Bolt_ID = Bolt_ID,
                    Adoazonosito = Adoazonosito,
                    Cim = Cim,
                    Nev = Nev,
                    Telefonszam = Telefonszam
                },
                Bolt_ID);
            }
        }
    }
}
