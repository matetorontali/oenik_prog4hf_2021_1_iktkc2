﻿using CD_Store.Data.Models;
using CD_Store.Logic;
using System.Collections.Generic;

namespace WpfApp.ViewModels
{
    public class BoltokListVM
    {
        private readonly ILogic logic;

        public BoltokListVM(/*ILogic logic*/)
        {
            this.logic = new Logic();
            Boltok = logic.GetBoltok();
        }

        public List<Bolt> Boltok { get; set; }
    }
}
