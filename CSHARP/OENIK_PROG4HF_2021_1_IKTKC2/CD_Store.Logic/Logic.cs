﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using CD_Store.Data.Models;
    using CD_Store.Repository;

    /// <summary>
    /// Contains the logic methods.
    /// </summary>
    public class Logic : ILogic
    {
        private readonly GenericRepository repository;

        /// <summary>
        /// Returns every vasarlas object.
        /// </summary>
        /// <returns>List of <see cref="Vasarlas"/>.</returns>
        public List<Vasarlas> GetVasarlas() => this.repository.Vasarlas.GetAll().ToList();

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        public Logic()
        {
            if (this.repository == null)
            {
                this.repository = new GenericRepository();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        /// <param name="moqrepo">The mock repository.</param>
        public Logic(GenericRepository moqrepo)
        {
            this.repository = moqrepo;
        }

        /// <summary>
        /// Deletes an object from a table.
        /// </summary>
        /// <returns>Sikeres delete if true.</returns>
        /// <param name="obj">Deletable object.</param>
        public string Delete(object obj)
        {
            if (obj is Bolt)
            {
                this.repository.Bolt.Delete(obj as Bolt);
            }
            else if (obj is Bolt_album)
            {
                this.repository.Bolt_Album.Delete(obj as Bolt_album);
            }
            else if (obj is Lemez)
            {
                this.repository.Lemez.Delete(obj as Lemez);
            }
            else if (obj is Vasarlas)
            {
                this.repository.Vasarlas.Delete(obj as Vasarlas);
            }
            else if (obj is Vevo)
            {
                this.repository.Vevo.Delete(obj as Vevo);
            }

            return "Sikeres delete!";
        }

        /// <summary>
        /// Returns every Bolt.
        /// </summary>
        /// <returns>List of <see cref="GetBoltok"/>.</returns>
        public List<Bolt> GetBoltok()
        {
            return this.repository.Bolt.GetAll().ToList();
        }

        /// <summary>
        /// Returns every bolt_album object.
        /// </summary>
        /// <returns>List of <see cref="Bolt_album"/>.</returns>
        public List<Bolt_album> GetBolt_Album()
        {
            return this.repository.Bolt_Album.GetAll().ToList();
        }

        /// <summary>
        /// Returns every lemez.
        /// </summary>
        /// <returns>List of <see cref="Lemez"/>.</returns>
        public List<Lemez> GetLemezek()
        {
            return this.repository.Lemez.GetAll().ToList();
        }

        /// <summary>
        /// Returns every vevo.
        /// </summary>
        /// <returns>List of <see cref="Vevo"/>.</returns>
        public List<Vevo> GetVevok()
        {
            return this.repository.Vevo.GetAll().ToList();
        }

        /// <summary>
        /// Inserts an object into a table.
        /// </summary>
        /// <returns>Sikeres insert if true.</returns>
        /// <param name="obj">Insertable object.</param>
        public string Insert(object obj)
        {
            if (obj is Bolt)
            {
                this.repository.Bolt.Insert(obj as Bolt);
            }
            else if (obj is Bolt_album)
            {
                this.repository.Bolt_Album.Insert(obj as Bolt_album);
            }
            else if (obj is Lemez)
            {
                this.repository.Lemez.Insert(obj as Lemez);
            }
            else if (obj is Vasarlas)
            {
                this.repository.Vasarlas.Insert(obj as Vasarlas);
            }
            else if (obj is Vevo)
            {
                this.repository.Vevo.Insert(obj as Vevo);
            }

            return "Sikeres insert!";
        }

        /// <summary>
        /// Updates an object from a table.
        /// </summary>
        /// <returns>Sikeres update if true.</returns>
        /// <param name="obj">Updated object.</param>
        /// <param name="key">Updateable object's key.</param>
        public string Update(object obj, object key)
        {
            if (obj is Bolt)
            {
                this.repository.Bolt.Update(obj as Bolt, key);
            }
            else if (obj is Bolt_album)
            {
                this.repository.Bolt_Album.Update(obj as Bolt_album, key);
            }
            else if (obj is Lemez)
            {
                this.repository.Lemez.Update(obj as Lemez, key);
            }
            else if (obj is Vasarlas)
            {
                this.repository.Vasarlas.Update(obj as Vasarlas, key);
            }
            else if (obj is Vevo)
            {
                this.repository.Vevo.Update(obj as Vevo, key);
            }

            return "Sikeres update!";
        }

        /// <summary>
        /// Returns how many albums in each store.
        /// </summary>
        /// <returns>List of <see cref="groupAdat"/>.</returns>
        public List<NonCrud> NonCrud1()
        {
            Console.WriteLine("Ekkora mennyiség van a boltokban az adott albumokból:");
            var list = (from bolt_album in this.repository.Bolt_Album.GetAll()
                        join bolt in this.repository.Bolt.GetAll() on bolt_album.Bolt_ID equals bolt.Bolt_ID
                        join lemez in this.repository.Lemez.GetAll() on bolt_album.Album equals lemez.Album
                        group lemez.Album by bolt.Nev
                        into groupAdat
                        select new NonCrud
                        {
                            Szoveg = groupAdat.Key,
                            SzamErtek = groupAdat.Count(),
                        }).ToList();

            return list;
        }

        /// <summary>
        /// Returns the maximum price of each store.
        /// </summary>
        /// <returns>List of <see cref="groupAdat"/>.</returns>
        public List<NonCrud> NonCrud2()
        {
            Console.WriteLine("Ennyibe kerülnek az adott albumok maximum a boltokban:");
            var list = (from bolt_album in this.repository.Bolt_Album.GetAll()
                        join bolt in this.repository.Bolt.GetAll() on bolt_album.Bolt_ID equals bolt.Bolt_ID
                        join lemez in this.repository.Lemez.GetAll() on bolt_album.Album equals lemez.Album
                        group bolt_album.Ar by bolt.Nev
                        into groupAdat
                        select new NonCrud
                        {
                            Szoveg = groupAdat.Key,
                            SzamErtek = groupAdat.Max(a => a.Value),
                        }).ToList();

            return list;
        }

        /// <summary>
        /// Returns the average spending of each person.
        /// </summary>
        /// <returns>List of <see cref="groupAdat"/>.</returns>
        public List<NonCrud> NonCrud3()
        {
            Console.WriteLine("Ennyit költöttek átlagosan a személyek:");
            var list = (from vasarlas in this.repository.Vasarlas.GetAll()
                        join vevo in this.repository.Vevo.GetAll() on vasarlas.V_ID equals vevo.V_ID
                        join bolt_album in this.repository.Bolt_Album.GetAll() on vasarlas.BA_ID equals bolt_album.BA_ID
                        group bolt_album by vevo
                       into groupAdat
                        select new NonCrud
                        {
                            Szoveg = groupAdat.Key.Nev,
                            SzamErtek = (int)groupAdat.Average(a => a.Ar),
                        }).ToList();

            return list;
        }

        /// <summary>
        /// Gets data from Java EndPoint.
        /// </summary>
        /// <returns>List of <see cref="string"/>.</returns>
        /// <param name="darabszam">Determines how many row should the endpoint generate.</param>
        public List<NonCrud> Java(int darabszam)
        {
            if (darabszam > 0)
            {
                try
                {
                    XDocument xdoc = XDocument.Load("http://localhost:8080/JAVA/LemezServlet?lemezSzam=" + darabszam);
                    return xdoc.Descendants("lemezek").Select(a => new NonCrud
                    {
                        Szoveg = a.Element("cim").Value,
                        SzamErtek = Convert.ToInt32(a.Element("ar").Value),
                    }).ToList();
                }
                catch (System.Exception ex)
                {
                    throw new Exception("Endpoint is not active!" + ex.Message);
                }
            }
            else
            {
                return new List<NonCrud>();
            }
        }
    }
}
