﻿// <copyright file="NonCrud.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Gives two object to NonCrud methods.
    /// </summary>
    public class NonCrud
    {
        /// <summary>
        /// Gets or sets an universal string property.
        /// </summary>
        public string Szoveg { get; set; }

        /// <summary>
        /// Gets or sets an universal int property.
        /// </summary>
        public int SzamErtek { get; set; }
    }
}
