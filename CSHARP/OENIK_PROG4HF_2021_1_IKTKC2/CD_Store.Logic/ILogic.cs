﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CD_Store.Logic
{
    using System.Collections.Generic;
    using CD_Store.Data.Models;

    /// <summary>
    /// Contains the requirement methods for Logic class.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Returns every lemez.
        /// </summary>
        /// <returns>List of <see cref="Lemez"/>.</returns>
        List<Lemez> GetLemezek();

        /// <summary>
        /// Returns every vevo.
        /// </summary>
        /// <returns>List of <see cref="Vevo"/>.</returns>
        List<Vevo> GetVevok();

        /// <summary>
        /// Gets returns every vasarlas.
        /// </summary>
        /// <returns>List of <see cref="Vasarlas"/>.</returns>
        List<Vasarlas> GetVasarlas();

        /// <summary>
        /// Returns every boltalbum.
        /// </summary>
        /// <returns>List of <see cref="Bolt_album"/>.</returns>
        List<Bolt_album> GetBolt_Album();

        /// <summary>
        /// Returns every bolt.
        /// </summary>
        /// <returns>List of <see cref="Bolt"/>.</returns>
        List<Bolt> GetBoltok();

        /// <summary>
        /// Inserts an object into a table.
        /// </summary>
        /// <returns>Sikeres insert if true.</returns>
        /// <param name="obj">Insertable object.</param>
        string Insert(object obj);

        /// <summary>
        /// Updates an object from a table.
        /// </summary>
        /// <returns>Sikeres update if true.</returns>
        /// <param name="obj">Updated object.</param>
        /// <param name="key">Updateable object's key.</param>
        string Update(object obj, object key);

        /// <summary>
        /// Updates an object from a table.
        /// </summary>
        /// <returns>Sikeres delete if true.</returns>
        /// <param name="obj">Updated object.</param>
        string Delete(object obj);
    }
}
