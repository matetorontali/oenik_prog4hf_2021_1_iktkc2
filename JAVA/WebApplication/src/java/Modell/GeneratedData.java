/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modell;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ahaje
 */
@XmlRootElement()
public class GeneratedData {
    @XmlElement()
    List<Lemez> lemezek;
    
    public GeneratedData() 
    {
        lemezek = new ArrayList();
    }
    
    public void AddLemez(Lemez lemez)
    {
        lemezek.add(lemez);
    }
}